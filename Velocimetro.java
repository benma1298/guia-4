import java.util.Random;

public class Velocimetro {
	
	/* Velocidad refiere a velocidad actual del automovil */
	private int velocidad;
	private int velocidad_maxima = 120;
	private int tiempo;
	private int distancia;
	
	Velocimetro(int velocidad, int tiempo, int distancia){
		
		this.velocidad = velocidad;
		this.tiempo = tiempo;
		this.distancia = distancia;
	}
	
	Velocimetro(){
		
		Random t = new Random();
		this.tiempo = t.nextInt(10)+1;
		
		this.velocidad = this.velocidad_maxima * 5/18;
		
		this.distancia = this.tiempo * this.velocidad;
	}
	
	public int getVelocidad() {
		return velocidad;
	}
	
	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}
	
	public int getVelocidadMaxima() {
		return this.velocidad_maxima;
	}

	public int getTiempo() {
		return tiempo;
	}

	public void setTiempo(int tiempo) {
		this.tiempo = tiempo;
	}

	public int getDistancia() {
		return distancia;
	}

	public void setDistancia(int distancia) {
		this.distancia = distancia;
	}

}
