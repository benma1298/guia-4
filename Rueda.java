import java.util.Random;

public class Rueda {
	
	private int desgaste = 100;
	private int deterioro;
	
	Rueda(int desgaste, int deterioro){
		
		this.desgaste = desgaste;
		this.deterioro = deterioro;

	}
	
	Rueda(){
		
		Random d = new Random();
		this.deterioro = d.nextInt(10)+1;
		this.desgaste = this.desgaste - this.deterioro;
		
	}

	public int getDesgaste() {
		return desgaste;
	}

	public void setDesgaste(int desgaste) {
		this.desgaste = desgaste;
	}

	public int getDeterioro() {
		return deterioro;
	}

	public void setDeterioro(int deterioro) {
		this.deterioro = deterioro;
	}

}
