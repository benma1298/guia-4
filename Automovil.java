import java.util.ArrayList;
import java.util.Scanner;

public class Automovil {
	
	private Motor motor;
	private Velocimetro velocimetro;
	private ArrayList<Rueda> rueda;
	private Estanque estanque;
	private Boolean encendido;
	private int sumaDistancia;
	private Double gasto;
	private Double actual_estanque;
	Scanner sc = new Scanner(System.in);
	
	Automovil(){
		this.rueda = new ArrayList<Rueda>();
		this.encendido = false;
	}
	
	public Motor getMotor() {
		return motor;
	}
	
	public void setMotor(Motor motor) {
		this.motor = motor;
	}

	public Velocimetro getVelocimetro() {
		return velocimetro;
	}

	public void setVelocimetro(Velocimetro velocimetro) {
		this.velocimetro = velocimetro;
	}

	public Estanque getEstanque() {
		return estanque;
	}

	public void setEstanque(Estanque estanque) {
		this.estanque = estanque;
	}

	public Boolean getEncendido() {
		return encendido;
	}

	public void setEncendido(Boolean encendido) {
		this.encendido = encendido;
	}

	public ArrayList<Rueda> getRueda() {
		return rueda;
	}

	public void setRueda(Rueda rueda) {
		this.rueda.add(rueda);
	}
	
	public int getSumaDistancia() {
		return sumaDistancia;
	}

	public void setSumaDistancia(int sumaDistancia) {
		this.sumaDistancia = sumaDistancia;
	}
	
	public void enciende_automovil() {
		this.encendido = true;
	}
	
	
	public void chequeo_ruedas() {
	
		for (Rueda r : this.rueda) {
			System.out.println(r.getDesgaste() + "%");
		}
	}
	
	public void rendimiento() {
		this.gasto = (this.velocimetro.getDistancia() / this.motor.getConsumo());
		//System.out.println(this.gasto);
		
		
		this.setActual_estanque(this.estanque.getVolumen() - this.gasto);
		System.out.println(this.getActual_estanque() + " litros");
		
	}
	public Double getGasto() {
		return gasto;
	}

	public void setGasto(Double gasto) {
		this.gasto = gasto;
	}
	
	public Double getActual_estanque() {
		return actual_estanque;
	}

	public void setActual_estanque(Double actual_estanque) {
		this.actual_estanque = actual_estanque;
	}
	
	public void reporte_automovil() {
		
		System.out.println("El automovil tiene un motor con cilindrada de " + this.motor.getCilindrada() + " cc");	
		System.out.println("El automovil ha recorrido: " + this.velocimetro.getDistancia() + " metros");
		System.out.println("La velocidad actual del automovil es " + this.velocimetro.getVelocidad() + " m/s");
		System.out.println("El estado actual del estanque es: ");
		rendimiento();
		System.out.println("Las condiciones de las ruedas son: ");
		chequeo_ruedas();
			
	}
	
	public void salida() {
		do {
			System.out.println("El auto esta encendido");
			System.out.println("Oprima cualquier tecla para mover el auto");
			String letra = sc.nextLine();
			System.out.println("El auto se mueve");
			reporte_automovil();
			this.sumaDistancia = this.sumaDistancia + this.velocimetro.getDistancia();
			
		}while(this.getActual_estanque() >= this.estanque.getVolumen());

	}

}
