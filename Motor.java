import java.util.Random;

public class Motor {
	
	private String cilindrada;
	private Double consumo;

	Motor(String cilindrada, int consumo){
	
		this.cilindrada = cilindrada;
	}
	
	Motor(){
		/* Random para determinar cilindrada */
		String cilindrada[] = new String[]{"1,2", "1,6"};
		this.cilindrada = cilindrada[new Random().nextInt(cilindrada.length)];
		
		if("1,2".equals(this.cilindrada)) {
			this.consumo = 20000.0;
			
		}
		
		if("1,6".equals(this.cilindrada)) {
			this.consumo = 14000.0;
			
		}else {}
		
	}
	
	public String getCilindrada() {
		return cilindrada;
	}

	public void setCilindrada(String cilindrada) {
		this.cilindrada = cilindrada;
	}

	public Double getConsumo() {
		return consumo;
	}

	public void setConsumo(Double consumo) {
		this.consumo = consumo;
	}
	
}
